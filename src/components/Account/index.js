import React from 'react';
import {StyleSheet, Text,View, Dimensions, TouchableOpacity} from 'react-native';
import {IconPesananAktif} from '../../assets';
import { WARNA_UTAMA, WARNA_WARNING1, WARNA_WARNING2, WARNA_ABU_ABU } from '../../utils/constant';

const Account = ({title, subtitle}) => {
  return (
    // Props {title, status}
    // TouchableOpacity untuk bisa di pencet
    <TouchableOpacity style={styles.container}>
      <View style={styles.text}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.subtitle}>{subtitle}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Account;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    padding: 17,
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    marginVertical: windowHeight*0.02,
    alignItems: 'center'
  },
  text: {
    marginLeft: windowWidth*0.02,
  },
  title: {
    fontSize: 18,
    fontFamily: 'TitilliumWeb-Bold'
  },
  subtitle: {
    fontSize: 14,
    fontFamily: 'TitilliumWeb-SemiBold',
    color: '#0B6623',
  }
});
