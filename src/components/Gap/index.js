import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

// Membuat function untuk mengatasi jarak
const Gap = ({height, width }) => {
    return (
        <View style={{ height : height, width : width}}/>
    )
}

export default Gap

const styles = StyleSheet.create({})
