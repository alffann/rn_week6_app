import React, { useEffect } from 'react'
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native'
import { SplashBackground, Logo } from '../../assets'

const Splash = ({ navigation }) => {
    // Props Navigation di dapat dari react-navigation yang sudah diinstall
    // useEffect yang akan dijalankan pertama kali.
    useEffect(() => {
        setTimeout( () => {
            navigation.replace('MainApp'); // replace supaya ketika tekan tombol back di aplikasi, maka akan keluar dari aplikasi
        }, 3000) // set waktu 3 detik untuk halaman splash tampil dan selanjutnya ke halaman beranda/home
    }, [navigation]);

    return (
       <ImageBackground source={SplashBackground} style={styles.background}>
           <Image source={Logo} style={styles.logo} />
       </ImageBackground>
    )
}

export default Splash

const styles = StyleSheet.create({
    background: {
        flex: 1, // supaya keseluruhan halaman tampilan terisi oleh background
        alignItems: 'center', // tampilan rata tengah
        justifyContent: 'center' // tampilan rata tengah
    },
    logo: {
        width: 222,
        height: 105
    }
})
