import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {PesananAktif} from '../../components/';
import {ScrollView} from 'react-native-gesture-handler';

const Pesanan = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.riwayatPesanan}>
          <Text style={styles.label}>Riwayat Pesanan</Text>
          <PesananAktif title="Pesanan No. 0002139" status="Sudah Selesai"/>
          <PesananAktif title="Pesanan No. 0002140" status="Sudah Selesai"/>
          <PesananAktif title="Pesanan No. 0002141" status="Belum Disetrika"/>
          <PesananAktif title="Pesanan No. 0002142" status="Masih Dicuci"/>
          <PesananAktif title="Pesanan No. 0002143" status="Masih Dicuci"/>
        </View>
        </ScrollView>
    )
}

export default Pesanan

const styles = StyleSheet.create({
    label: {
      fontSize: 18,
      fontFamily: 'TitilliumWeb-Bold',
    },
    riwayatPesanan: {
      paddingTop: 10,
      paddingHorizontal: 30,
      
    },
  });
  