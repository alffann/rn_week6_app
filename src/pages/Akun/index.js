// import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
// import { Account } from '../../components/';

// const Akun = () => {
//     return (
//         <Text>Akun</Text>
//     )
// }

// export default Akun

// const styles = StyleSheet.create({})

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Account } from '../../components/';
import { ScrollView } from 'react-native-gesture-handler';

const Akun = () => {
    return (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.akun}>
          <Text style={styles.label}>Akun Antar</Text>
          <Account title="Username" subtitle="Alif Irfan"/>
          <Account title="Nomor Handphone" subtitle="08522926xxxx"/>
          <Account title="Email" subtitle="alif.irfan@gmail.com"/>
          <Account title="Saldo" subtitle="Rp. 500.000"/>
          <Account title="Antar Point" subtitle="800 points"/>
        </View>
        </ScrollView>
    )
}

export default Akun

const styles = StyleSheet.create({
    label: {
      fontSize: 18,
      fontFamily: 'TitilliumWeb-Bold',
    },
    akun: {
      paddingTop: 10,
      paddingHorizontal: 30,
    },
  });
  
