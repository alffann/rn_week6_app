export const WARNA_UTAMA = '#55CB95';    // Warna active atau green
export const WARNA_DISABLE = '#C8C8C8';  // Warna non-active atau abu-abu icon
export const WARNA_SEKUNDER = "#E0F7EF"; // Warna background icon
export const WARNA_ABU_ABU = '#F6F6F6';  // Warna background abu-abu
export const WARNA_WARNING1 =  '#FF4D00'; // Warna untuk keterangan masih dicuci
export const WARNA_WARNING2 =  '#800080'; // Warna untuk keterangan belum disetrika